# 个人算法题集锦（不保正确，共赏共勉）

*在此感谢爪哇教育，免费让我挑的一本书，我选了左程云的《程序员代码面试指南-IT名企算法与数据结构题目最优解》，早些年在图书馆借过，感觉挺好的，下方题目来源于此书。


1. [用两个栈实现一个队列。](1.md)
2. [如何使用一个递归函数和栈操作逆序一个栈](2.md)
3. [生成窗口最大值数组](3.md)